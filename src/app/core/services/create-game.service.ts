import { Injectable } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore'

@Injectable({
  providedIn: 'root'
})
export class CreateGameService {
  idGame:string
  gamerName1:string='Jugador 1'
  gamerName2:string='Jugador 2'
  constructor(
    private firestore : AngularFirestore
  ) { }
 
  gatGame(id:any){  /*get from database the game*/
    return this.firestore.collection('games').doc(id).snapshotChanges()
   }
  postGame(game:any){ /*create game in database*/
   return this.firestore.collection('games').add(game)
  }
  updateGame(id, game:any){ /*update game data*/
    return this.firestore.collection('games').doc(id).update(game)
  }
}
