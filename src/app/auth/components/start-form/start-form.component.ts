import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CreateGameService } from 'src/app/core/services/create-game.service';
import { FormControl, FormBuilder, Validators} from '@angular/forms';
@Component({
  selector: 'app-start-form',
  templateUrl: './start-form.component.html',
  styleUrls: ['./start-form.component.sass']
})
export class StartFormComponent implements OnInit {
  form = this.fb.group({
    player1: null,
    player2: null,
  });
  queryId: string
  constructor(private router: Router,  
              public dialog:MatDialogRef<StartFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private CreateGameService:CreateGameService, private fb: FormBuilder) { }

  ngOnInit(): void {
  }
  
  goBoard(){ /*create game in database with form*/
   if(this.form.get('player2').value == null){
    this.form.get('player2').setValue('jugador2')
   }
   if(this.form.get('player1').value == null){
    this.form.get('player1').setValue('jugador1')
   }
    this.CreateGameService.postGame(this.form.value).then(res =>{
      this.router.navigate(['board'], { queryParams: { id: res.id} } );
      this.dialog.close()
    }).catch(err=>{
      console.error(err)
    }) 
  }

  joinBoard(){  /*join en a existing game */
   let updateData={
    player2:this.form.get('player2').value
   }
    this.CreateGameService.updateGame( this.queryId,  updateData).then(res =>{
      this.router.navigate(['board'], { queryParams: { id: this.queryId} } );
      this.dialog.close()
    }).catch(err=>{
      console.error(err)
    }) 
  }
}
