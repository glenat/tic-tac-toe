import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { StartFormComponent } from '../start-form/start-form.component';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.sass']
})
export class StartComponent implements OnInit {
  
  constructor(public dialog: MatDialog,) { }

  ngOnInit(): void {
  }
  openDialog(option){ /*open game form*/
    const dialogRef = this.dialog.open(StartFormComponent,{
      data:{option:option}
    });
  }

}
