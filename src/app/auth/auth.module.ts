import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartComponent } from './components/start/start.component';
import { MaterialModule } from '../material/material/material.module';
import { StartFormComponent } from './components/start-form/start-form.component';



@NgModule({
  declarations: [StartComponent, StartFormComponent],
  imports: [
    CommonModule,
    MaterialModule
  ]
})
export class AuthModule { }
