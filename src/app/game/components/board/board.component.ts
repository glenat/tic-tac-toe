import { Component, OnInit } from '@angular/core';
import { CreateGameService } from 'src/app/core/services/create-game.service';
import { Router, ActivatedRoute } from '@angular/router';
import {map, startWith, switchMap} from 'rxjs/operators';
export interface Tile {
  color: string;
  cols: number;
  rows: number;
  value: string;
}
@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.sass']
})
export class BoardComponent implements OnInit {
  tiles: Tile[] = [
    {value: null, cols: 1, rows: 1, color: 'lightblue'},
    {value: null, cols: 1, rows: 1, color: 'lightblue'},
    {value: null, cols: 1, rows: 1, color: 'lightblue'},
    {value: null, cols: 1, rows: 1, color: 'lightblue'},
    {value: null, cols: 1, rows: 1, color: 'lightblue'},
    {value: null, cols: 1, rows: 1, color: 'lightblue'},
    {value: null, cols: 1, rows: 1, color: 'lightblue'},
    {value: null, cols: 1, rows: 1, color: 'lightblue'},
    {value: null, cols: 1, rows: 1, color: 'lightblue'},
  ];
  firstTurn: boolean= true
  isTurn: boolean 
  theWinner: string
  statusMessage: string ='Ganador'
  game: any={
    player1 :null,
    player2:null
  }
  turnUserName:string
  id: string
  constructor(private CreateGameService:CreateGameService, public route:ActivatedRoute) { }

  ngOnInit(): void {
    this.isTurn= this.firstTurn
    this.viewGame()
  }
  viewGame(){ /*get game data from database*/
    this.route.queryParamMap
    .pipe(
      switchMap(params=>{
        this.id= params.get('id');
        return this.CreateGameService.gatGame(this.id)
      })
    ).subscribe(res=>{
      this.game=res.payload.data()
    })
   
  }
  resetGame(){ /*restart game*/
    this.firstTurn = !this.firstTurn;
    this.tiles.forEach(element => {
      element.value= null
    });
    console.log(this.firstTurn)
  }
  setMove(i){ /*set move in tile and checkout if have winner*/
  
    this.tiles[i].value=this.turn
    this.isTurn = !this.isTurn;
    this.theWinner= this.checkWinner()
    if(this.emptyCell == false && this.checkWinner() == null){
      this.statusMessage= 'Empate'
    }
    console.log(this.turn)
  }


  get turn() { /*switch the game turn*/
    return this.isTurn ? 'X' : 'O';
  }

  get emptyCell(){ /*checkout if have empty tile*/
    let empty = false
    this.tiles.forEach(element => {
      if(element.value == null){
        empty = true 
        return empty
      }
    });
    return empty
  }
  checkWinner(){ /*checkout if the game was won*/
    let lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      let [a, b, c] = lines[i];
      if (this.tiles[a].value &&
          this.tiles[a].value === this.tiles[b].value &&
          this.tiles[a].value === this.tiles[c].value){
        return this.tiles[a].value;
      }
    }
    return null;
  }
  
}
