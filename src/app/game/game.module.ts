import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardComponent } from './components/board/board.component';
import { MaterialModule } from '../material/material/material.module';



@NgModule({
  declarations: [BoardComponent],
  imports: [
    CommonModule,
    MaterialModule
  ]
})
export class GameModule { }
