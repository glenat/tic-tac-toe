import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { StartComponent } from './auth/components/start/start.component';
import { BoardComponent } from './game/components/board/board.component';


const routes: Routes = [
  { path: 'board', component: BoardComponent },
  { path: 'home', component: StartComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
]
@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
